# README #

Agent for synchronizing folders with Sia.

## Downloads ##

https://bitbucket.org/stortech/siasync-agent/downloads/

## Configuration ##

Currently the only mode implemented is a simple name based sync.  Files that are updated will not be re-synced.

If you run the file without a `config.json` file in the same directory, it will create an empty one and then exit.

The configuration is handled by a json file called `config.json` in she same directory as the executable.
The `poll_interval` parameter determines how often in seconds the sync runs.
The `location` parameter is the local folder you are syncing to Sia.
The `siabase` parameter is the folder in Sia it will be synced too.
The `delete` parameter indicates whether files on Sia that no longer exist locally should be deleted.

```json
{
  "poll_interval": 15,
  "folders": [
    {
      "location": "C:\\Location",
      "siabase": "TestFolder",
      "delete": true
    }
  ]
}
```

## License ##
MIT