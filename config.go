package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"time"
)

func loadConfig() (Config, error) {
	config := Config{}
	file, err := os.Open("config.json")
	if err != nil {
		return config, err
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)
	return config, err
}

func generateDefaultConfig() {
	config := Config{}
	folders := []folder{folder{}}
	config.Folders = folders

	configJSON, _ := json.MarshalIndent(config, "", "  ")
	ioutil.WriteFile("config.json", configJSON, 0644)
}

// Config - Configuration of the agent
type Config struct {
	PollInterval time.Duration `json:"poll_interval"`
	Folders      []folder      `json:"folders"`
}

type folder struct {
	Location string `json:"location"`
	SiaBase  string `json:"siabase"`
	Delete   bool   `json:"delete"`
}
