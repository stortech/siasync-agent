package client

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"
)

// Client - client for interacting with Sia
type Client struct {
	client   *http.Client
	endpoint string
}

// NewClient - initializes a Client
func NewClient() *Client {
	tr := &http.Transport{
		MaxIdleConns:       10,
		IdleConnTimeout:    3 * time.Second,
		DisableCompression: true,
	}
	client := &http.Client{Transport: tr}
	endpoint := "http://localhost:9980/"
	return &Client{client: client, endpoint: endpoint}
}

// Get - get data from SIA api
func (c *Client) Get(path string, output interface{}) error {
	var fullPath = c.endpoint + path

	req, err := http.NewRequest("GET", fullPath, nil)
	if err != nil {
		return err
	}
	req.Header.Set("User-Agent", "Sia-Agent")

	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, &output)
	if err != nil {
		return err
	}
	return nil
}

// Upload - upload the file at localPath to the location specificed by siapath
func (c *Client) Upload(localPath string, siapath string) error {
	var query = "?source=" + localPath

	var fullPath = c.endpoint + "renter/upload/" + siapath + query
	req, err := http.NewRequest("POST", fullPath, nil)
	if err != nil {
		return err
	}
	req.Header.Set("User-Agent", "Sia-Agent")

	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return errors.New("Failed to upload: " + localPath + " " + resp.Status)
	}
	return nil
}

// Delete - delete the file at the siapath
func (c *Client) Delete(siapath string) error {
	var fullPath = c.endpoint + "renter/delete/" + siapath
	req, err := http.NewRequest("POST", fullPath, nil)
	if err != nil {
		return err
	}
	req.Header.Set("User-Agent", "Sia-Agent")
	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return errors.New("Failed to delete: " + siapath + " " + resp.Status)
	}
	return nil
}
