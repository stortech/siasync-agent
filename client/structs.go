package client

// Renter = response to /renter
type Renter struct {
	Settings         settings `json:"settings"`
	ContractSpending int      `json:"currentperiod"`
}

// RenterFiles = response to /renter/files
type RenterFiles struct {
	Files []File `json:"files"`
}

type allowance struct {
	Funds       string `json:"funds"`
	Hosts       int    `json:"hosts"`
	Period      int    `json:"period"`
	RenewWindow int    `json:"renewwindow"`
}

// File -  sia file description
type File struct {
	Siapath        string  `json:"siapath"`
	Filesize       int64   `json:"filesize"`
	Available      bool    `json:"available"`
	Renewing       bool    `json:"renewing"`
	Redundancy     float64 `json:"redundancy"`
	Uploadprogress float64 `json:"uploadprogress"`
	Expiration     int     `json:"expiration"`
}

type settings struct {
	Allowance allowance `json:"allowance"`
}
