package main

import (
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/sirupsen/logrus"

	"bitbucket.org/stortech/siasync-agent/client"
)

var log = logrus.New()

func main() {

	// file, err := os.OpenFile("siasync.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	// if err == nil {
	// 	log.Out = file
	// 	log.Formatter = &logrus.TextFormatter{FullTimestamp: true, DisableColors: true}
	// } else {
	// 	log.Formatter = &logrus.TextFormatter{FullTimestamp: true}
	// 	log.Info("Failed to log to file, using default stderr")
	// }
	// defer file.Close()

	config, err := loadConfig()
	if err != nil {
		log.Println(err)
		generateDefaultConfig()
		log.Fatal("Failed to log config.json.  Attempted to generate config.json")
	}

	siaClient := client.NewClient()

	// Run once at startup
	sync(siaClient, config)

	ticker := time.NewTicker(config.PollInterval * time.Second)
	for {
		select {
		case <-ticker.C:
			sync(siaClient, config)
		}
	}
}

func sync(siaClient *client.Client, config Config) {
	// Loop through synced folders
	for _, folder := range config.Folders {
		log.Println("Syncing Folder: " + folder.Location)
		_, err := os.Stat(folder.Location)
		if err != nil {
			log.Println(err)
			break
		}

		localFiles := folderFiles(folder.Location)
		var siaFiles = new(client.RenterFiles)
		err = siaClient.Get("renter/files", siaFiles)
		if err != nil {
			log.Println(err)
		}
		folderSiaFiles := client.SiaPathFiles(folder.SiaBase, siaFiles.Files)

		_, localOnly, siaOnly := Intersection(folderSiaFiles, localFiles, folder.SiaBase)

		for _, file := range localOnly {
			log.Println("Uploading: " + file.PathRel)
			err := siaClient.Upload(file.PathAbs, (folder.SiaBase + "/" + file.PathRel))
			if err != nil {
				log.Println(err)
			}
		}

		for _, file := range siaOnly {
			if folder.Delete {
				log.Println("Deleting: " + file.Siapath)
				err := siaClient.Delete(file.Siapath)
				if err != nil {
					log.Println(err)
				}
			}
		}
	}
}

func folderFiles(base string) (files []LocalFile) {
	filepath.Walk(base, func(path string, info os.FileInfo, _ error) error {
		if !info.IsDir() {
			relPath, err := filepath.Rel(base, path)
			if err != nil {
				return err
			}
			relPath = strings.Replace(relPath, "\\", "/", -1)
			newFile := LocalFile{PathAbs: path, PathRel: relPath}
			files = append(files, newFile)
		}
		return nil
	})
	return files
}

// Intersection - Find the interserction of uploaded files and local files.
func Intersection(siaFiles []client.File, folderFiles []LocalFile, siaBase string) ([]client.File, []LocalFile, []client.File) {
	// This obviously should be massively optimized.  Just need to write good tests first.
	var bothLocations []client.File
	var localOnly []LocalFile
	var siaOnly []client.File

	for _, folderFile := range folderFiles {
		var found = false
		for _, siaFile := range siaFiles {
			if client.RemoveSiaPath(siaFile.Siapath, siaBase) == folderFile.PathRel {
				bothLocations = append(bothLocations, siaFile)
				found = true
				break
			}
		}
		if !found {
			localOnly = append(localOnly, folderFile)
		}
	}

	for _, siaFile := range siaFiles {
		var found = false
		for _, folderFile := range folderFiles {
			if client.RemoveSiaPath(siaFile.Siapath, siaBase) == folderFile.PathRel {
				found = true
				break
			}
		}
		if !found {
			siaOnly = append(siaOnly, siaFile)
		}
	}

	return bothLocations, localOnly, siaOnly
}

// LocalFile - struct for storing info about filesystem files
type LocalFile struct {
	PathAbs string
	PathRel string
}
