package client

import (
	"regexp"
)

// SiaPathFiles -
func SiaPathFiles(folderName string, files []File) []File {
	var filteredFiles []File
	re := regexp.MustCompile(`^` + folderName + `.*`)
	for _, file := range files {
		if re.MatchString(file.Siapath) {
			filteredFiles = append(filteredFiles, file)
		}
	}
	return filteredFiles
}

// RemoveSiaPath - gives a relative path minus
func RemoveSiaPath(fileName string, folderName string) string {
	re := regexp.MustCompile(`^` + folderName + `/(.*)`)
	return re.ReplaceAllString(fileName, "$1")
}
